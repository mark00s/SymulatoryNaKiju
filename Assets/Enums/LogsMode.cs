﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum LogsMode {
    Pause = 0,
    Backward = 1,
    Forward = 2
}
