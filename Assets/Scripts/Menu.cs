﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;



public class Menu : MonoBehaviour
{
    public Button startButton;
    public Button exitButton;
    public Button PIRACI;

    void Start()
    {
        startButton.onClick.AddListener(startButtonEvent);
        exitButton.onClick.AddListener(exitButtonEvent);
        PIRACI.onClick.AddListener(PIRACIEvent);
    }

    void startButtonEvent()
    {
        SceneManager.LoadScene(1);
    }

    void exitButtonEvent()
    {
        Application.Quit();

    }

    void PIRACIEvent()
    {
        SceneManager.LoadScene(2);
    }



}
