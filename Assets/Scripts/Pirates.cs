﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pirates : MonoBehaviour {

    public Terrain terrain1;
    public Terrain terrain2;

    public float x1;
    public float x2;

	// Use this for initialization
	void Start () {
        x1 = terrain1.transform.position.x;
        x2 = terrain2.transform.position.x;
	}
	
	// Update is called once per frame
	void Update () {
        x1 = Mathf.MoveTowards((float)x1, -50f, 5f * Time.deltaTime);
        x2 = Mathf.MoveTowards((float)x2, -455f, 5f * Time.deltaTime);

        terrain1.transform.position = new Vector3(x1, terrain1.transform.position.y, terrain1.transform.position.z);
        terrain2.transform.position = new Vector3(x2, terrain2.transform.position.y, terrain2.transform.position.z);
	}
}
