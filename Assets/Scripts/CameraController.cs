﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CameraController : MonoBehaviour {

    public GameObject [] Camera = new GameObject[5];

    void switchCamera(int camID)
    {
        for (int i = 0; i < 5; i++)
        {
            Camera[i].SetActive(i == camID);
        }
    }

	void Update () {
        if (Input.GetKeyDown(KeyCode.F1))
        {
            switchCamera(0);
        }
        if (Input.GetKeyDown(KeyCode.F2))
        {
            switchCamera(1);
        }
        if (Input.GetKeyDown(KeyCode.F3))
        {
            switchCamera(2);
        }
        if (Input.GetKeyDown(KeyCode.F4))
        {
            switchCamera(3);
        }
        if (Input.GetKeyDown(KeyCode.F5))
        {
            switchCamera(4);
        }
    }
}
