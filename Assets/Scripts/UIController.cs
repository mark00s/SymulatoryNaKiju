﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System.IO;
using UnityEngine.SceneManagement;


public class UIController : MonoBehaviour {

    
    //TEXT
    public TextMeshProUGUI cogText;
    public TextMeshProUGUI leftRightText;
    public TextMeshProUGUI logText;
    //INPUT
    public TMP_InputField logField;
    //BUTTON
    public Button logButton;
    public Button saveButton;
    public Button loadButton;
    public Button pauseButton;
    public Button endButton;
    //GAMEOBJECT
    public GameObject boatObject;
    public GameObject echoSounder;
    //RECTTRANSFORM
    public RectTransform compass;
    //SKRYPTY
    Movement boatScript;
    EchoSounder echoSounderScript;
    
    //ZMIENNE
    bool logState = false;
    double unit = 10; //Jedna jednostka w Unity ile to metrów

	void Start ()
    {
        boatScript = boatObject.GetComponent<Movement>();
        echoSounderScript = echoSounder.GetComponent<EchoSounder>();
        logText.gameObject.SetActive(false);
        endButton.gameObject.SetActive(false);
        logGUI(false);
        logButton.onClick.AddListener(logButtonClick);
        saveButton.onClick.AddListener(saveButtonClick);
        loadButton.onClick.AddListener(loadButtonClick);
        pauseButton.onClick.AddListener(pauseButtonClick);
        endButton.onClick.AddListener(endButtonClick);
        StartCoroutine(Params());
	}

    //AKTUALIZACJA PARAMETRÓW
    IEnumerator Params()
    {
        while (true)
        {
            double cogUI = boatScript.Cog;
            while (cogUI < 0) cogUI = 360 + cogUI;
            while (cogUI > 360) cogUI = cogUI - 360;

            cogText.text = string.Format("COG: {0}\u00B0\nROT: {1:0.00}\u00B0\nES: {2:0.00}m\nSPEED: {3:0.0}kn", (int)cogUI, boatScript.Rot, echoSounderScript.Hit.distance * unit, boatScript.Velocity * unit * 0.5144);
            yield return new WaitForSeconds(2f);
        }
    }

    //POJAWIANIE SIĘ I ZNIKANIE GUI
    void logGUI(bool state)
    {
        saveButton.gameObject.SetActive(state);
        loadButton.gameObject.SetActive(state);
        pauseButton.gameObject.SetActive(state);
        logField.gameObject.SetActive(state);
        logState = state;
    }

    //PRZYCISK DO GUI
    void logButtonClick()
    {
        logGUI(!logState);
    }

    //ZAPIS LOGÓW DO PLIKU
    void saveButtonClick()
    {
        using (StreamWriter writer = new StreamWriter(logField.text))
        {
            writer.Write(boatScript.LOG);
        }

        string str = logField.text;
        string[] filename = str.Split('.');
        filename[0] += "_r.txt";

        using (StreamWriter writer = new StreamWriter(filename[0]))
        {
            writer.Write("T\t\tX\t\tZ\t\tCOG\tROT\tSR\tV");
            writer.Write(boatScript.LOG_R);
        }
    }

    //WCZYTYWANIE TRYBU LOGOWANIA
    void loadButtonClick()
    {
        logText.gameObject.SetActive(true);
        boatScript.LOG = "";

        using (StreamReader reader = new StreamReader(logField.text))
        {
            boatScript.LOG = reader.ReadLine();
        }

        boatScript.loadLogMode();
    }

    //KONIEC TRYBU LOGOWANIA
    void pauseButtonClick()
    {
        logGUI(!logState);
        logText.gameObject.SetActive(false);
        boatScript.pauseLogMode();
    }

    //KONIEC SYMULACJI
    void endButtonClick()
    {
        SceneManager.LoadScene(0);
    }
	
	void Update () {
        compass.rotation = Quaternion.Euler(0, 0, (float)boatScript.Cog);
        leftRightText.text = string.Format("{0:0.0}", boatScript.SR);

        if (!boatScript.LogMode)
        {
            if (boatScript.Win == 1)
            {
                endButton.gameObject.SetActive(true);
                endButton.GetComponentInChildren<Text>().text = "Wygrałeś!";
                endButton.GetComponent<Image>().color = Color.green;
            }
            else if (boatScript.Win == 2)
            {
                endButton.gameObject.SetActive(true);
                endButton.GetComponentInChildren<Text>().text = "Kolizja!";
                endButton.GetComponent<Image>().color = Color.red;
            }
        }
    }


}
