﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Keys : MonoBehaviour {


    public Slider upDownSlider;
    public Slider leftRightSlider;
    Movement movementScript;
    GameObject boat;

    void Start()
    {
        boat = this.gameObject;
        movementScript = boat.GetComponent<Movement>();
    }

	void Update () {
        //W
		if (Input.GetKeyDown(KeyCode.W))
        {
            upDownSlider.value += 10;
        }

        //S
        if (Input.GetKeyDown(KeyCode.S))
        {
            if (!movementScript.LogMode)
                upDownSlider.value -= 10;
            else
                movementScript.LogKey = 1;
        }

        //A
        if (Input.GetKeyDown(KeyCode.A))
        {
            if (!movementScript.LogMode)
                leftRightSlider.value -= 5;
            else
                movementScript.LogKey = 2;
        }   

        //D
        if(Input.GetKeyDown(KeyCode.D))
        {
            if (!movementScript.LogMode)
                leftRightSlider.value += 5;
            else
                movementScript.LogKey = 0;
        }

        //SPACJA
        if(Input.GetKeyDown(KeyCode.Space))
        {
            leftRightSlider.value = 0;
        }

        //ESCAPE
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene(0);
        }
    }  
}
