﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using UnityEngine;
using UnityEngine.UI;


public class Movement : MonoBehaviour {

    MyShip ship;

    //LOGOWANIE
    bool logMode = false;
    int logKey = 0; //0 - Naprzod / 1- Stop / 2 - Do tyłu
    int win = 0; //0 - Gra / 1 - Wygrana 2 - Przegrana
    string log;
    string log_r;
    string[] logs;
    int logIterator;
    int clock = 0;
    public Slider upDownSlider;
    public Slider leftRightSlider;

    //NOMOTO
    public double T;
    public double k;
    public double speed;
    double Sc = -0.0159;

    //STATEK
    double v;
    double x;
    double z;
    double Sr;
    double cog;
    double rot;
    double velocity;
    Vector3 previousPosition;

    //AKCESORY
    public double Rot { get { return rot; }}
    public double Velocity { get { return velocity; }}
    public double SR { get { return Sr; } }
    public double V { get { return v; } }
    public double X { get { return x; } }
    public double Z { get { return z; } }
    public string LOG_R { get { return log_r; } }
    public int Win { get { return win; } }
    public double Cog { get { return cog; } set { cog = value; } }
    public bool LogMode { get { return logMode; } set { logMode = value; } }
    public int LogKey { get { return logKey; } set { logKey = value; } }
    public string LOG { get { return log; } set { log = value; } }



    void Start()
    {

        ship = new MyShip(gameObject);
        ship.LoadSliders(leftRightSlider, upDownSlider);

       /* x = transform.position.x;
        z = transform.position.z;
        cog = transform.eulerAngles.y
        */
        log = "";
        StartCoroutine(Logs());
    }


    //ZAPISYWANIE LOGÓW
    IEnumerator Logs()
    {
        while (true)
        {
            ship.updateLogs();
            yield return new WaitForSeconds(1f);
        }
    }

    //WCZYTYWANIE TRYBU LOGOWANIA
    public void loadLogMode()
    {
        logs = log.Split(':');
        logIterator = 0;
        logMode = true;
    }

    //KOŃCZENIE TRYBU LOGOWANIA
    public void pauseLogMode()
    {
        log = "";
        log_r = "";
        x = 0; z = 0; cog = 0; Sr = 0;
        upDownSlider.value = 0; leftRightSlider.value = 0;
        logMode = false;
    }

    //KOLIZJA
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Win") && (win != 2))
        {
            win = 1;
        }
        else if (other.CompareTag("Terrain"))
        {
            win = 2;
        }
    }


    void FixedUpdate()
    {
        /*
        //TRANSFORM
        transform.position = new Vector3((float)x, transform.position.y, (float)z);
        transform.rotation = Quaternion.Euler(0, (float)(cog), 0);
        */

        

        if (logMode)
        {
            //TRYB LOGOWANIA
            if (logKey == 0)
            {
                if (logIterator + 7 < logs.Length)
                {
                    //ZMIANA POZYCJI
                    logIterator += 3;
                    x = Mathf.MoveTowards(float.Parse(logs[logIterator]), float.Parse(logs[logIterator + 3]), Time.deltaTime);
                    z = Mathf.MoveTowards(float.Parse(logs[logIterator + 1]), float.Parse(logs[logIterator + 4]), Time.deltaTime);
                    cog = Mathf.MoveTowards(float.Parse(logs[logIterator + 2]), float.Parse(logs[logIterator + 5]), Time.deltaTime);

                    
                }
            }
            else if (logKey == 2)
            {
                if (logIterator > 0)
                {
                    //ZMIANA POZYCJI
                    logIterator -= 3;
                    x = Mathf.MoveTowards(float.Parse(logs[logIterator + 3]), float.Parse(logs[logIterator]), Time.deltaTime);
                    z = Mathf.MoveTowards(float.Parse(logs[logIterator + 4]), float.Parse(logs[logIterator + 1]), Time.deltaTime);
                    cog = Mathf.MoveTowards(float.Parse(logs[logIterator + 5]), float.Parse(logs[logIterator + 2]), Time.deltaTime);
             
                }
            }
        }
        else
        {
            ship.UpdateShipPosition();
            /*
             
            //SLIDERY
            Sr = Mathf.MoveTowards((float)Sr, leftRightSlider.value, 1.5f * Time.deltaTime * 5);
            v = Mathf.MoveTowards((float)v, upDownSlider.value, 0.5f * Time.deltaTime * 5); 
              
            //PRĘDKOŚC
            velocity = ((transform.position - previousPosition).magnitude) / Time.deltaTime;
            previousPosition = transform.position;

            //NOMOTO
            rot += (k * (Sc + Sr) - rot) * Time.deltaTime / T;
            cog += rot * Time.deltaTime;
            x += v * Time.deltaTime * speed * Mathf.Sin((float)(cog / 57.3));
            z += v * Time.deltaTime * speed * Mathf.Cos((float)(cog / 57.3));
            */

        }
    }
}
