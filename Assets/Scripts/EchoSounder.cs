﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EchoSounder : MonoBehaviour {
    private RaycastHit hit;
    public RaycastHit Hit { get { return hit; } }

	void FixedUpdate () {
        Physics.Raycast(transform.position, Vector3.down, out hit);
	}
}
