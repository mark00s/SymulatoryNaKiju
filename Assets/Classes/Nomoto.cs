﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    public class Nomoto
    {
    private double T = 50.66;
    private double k = 0.07;
    private double Sc = -0.0159;


    public double NewRot(double oldRot, double Sr)
    {
        return (k * (Sc + Sr) - oldRot) * Time.deltaTime / T;
    }

}


