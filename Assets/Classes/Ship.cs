﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Ship
{

    //PARAMETRY STATKU
    protected double v;
    protected double x;
    protected double z;
    protected double sr;
    protected double cog;
    protected double rot;
    protected double velocity;
    protected GameObject shipObject;


    public Ship(GameObject shipObject)
    {
        this.shipObject = shipObject;
        x = shipObject.transform.position.x;
        z = shipObject.transform.position.z;
        cog = shipObject.transform.eulerAngles.y;
    }

    public abstract void UpdateShipPosition();

}