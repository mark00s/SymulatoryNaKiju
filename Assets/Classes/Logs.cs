﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Logs {
    public bool LogModeEnabled = false;
    LogsMode Mode = LogsMode.Pause;
    int logIterator = 0;

    string log; // Logi które są wykorzystywane przez symulator
    string log_r; // Logi które są do odczytu
    string [] logArray;



    int clock; //Zegar pokazujący czas w sekundach

    public void Update(double x, double z, double cog, double rot, double sr, double velocity)
    {
        if (!LogModeEnabled)
        {
            log += String.Format("{0:0.000}:{1:0.000}:{2:0.000}:", x, z, cog);
            log_r += String.Format("\n{0}\t\t{1:0.0}\t\t{2:0.0}\t\t{3:0}\t{4:0.0}\t{5:0}\t{6:0.0}", clock, x, z, cog, rot, sr, velocity);
            clock++;
        }
    }

    public double Movement(int parameter)//0 - x, 1 - z, 2 - cog
    {
        return float.Parse(logArray[logIterator+parameter]);
    }

    public void UpdateIterator()
    {
        if (Mode == LogsMode.Forward)
            logIterator += 3;
        else if (Mode == LogsMode.Backward)
            logIterator -= 3;
    }

    public bool OutOfRange()
    {
        if ((Mode == LogsMode.Forward && logIterator + 7 >= log.Length) || (Mode == LogsMode.Backward && logIterator == 0)) // 7 to za dużo, trzeba przetestować i zmniejszyć
            return true;
        return false;
    }

    public void StartLogMode()
    {
        logArray = log.Split(':');
        logIterator = 0;
        LogModeEnabled = true;
    }

    public void StopLogMode()
    {
        log = "";
        log_r = "";
        LogModeEnabled = false;
    }
}