﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MyShip : Ship {

    Nomoto nomoto;
    Logs logs;

    double speed = 0.047; //Prędkość maxymalna statku
    Vector3 previousPosition;

    private Slider leftRightSlider;
    private Slider upDownSlider;

    public MyShip(GameObject shipObject) : base(shipObject)
    {
        nomoto = new Nomoto();
        logs = new Logs();
    }

    public void LoadSliders(Slider leftRightSlider, Slider upDownSlider)
    {
        this.leftRightSlider = leftRightSlider;
        this.upDownSlider = upDownSlider;
    }

    public override void UpdateShipPosition()
    {
        if (!logs.LogModeEnabled)
        {
            //POZYCJA STATKU
            shipObject.transform.position = new Vector3((float)x, shipObject.transform.position.y, (float)z);
            shipObject.transform.rotation = Quaternion.Euler(0, (float)(cog), 0);


            //SLIDERY
            sr = Mathf.MoveTowards((float)sr, leftRightSlider.value, 1.5f * Time.deltaTime * 5);
            v = Mathf.MoveTowards((float)v, upDownSlider.value, 0.5f * Time.deltaTime * 5);

            //PRĘDKOŚC
            velocity = ((shipObject.transform.position - previousPosition).magnitude) / Time.deltaTime;
            previousPosition = shipObject.transform.position;

            //NOMOTO
            rot += nomoto.NewRot(rot, sr);
            cog += rot * Time.deltaTime;
            x += v * Time.deltaTime * speed * Mathf.Sin((float)(cog / 57.3));
            z += v * Time.deltaTime * speed * Mathf.Cos((float)(cog / 57.3));
        }
        else
        {
            if(!logs.OutOfRange())
            {
                x = logs.Movement(0);
                z = logs.Movement(1);
                cog = logs.Movement(2);
                logs.UpdateIterator();
            }
        }
    }


    public void StartLogMode()
    {
        logs.StartLogMode();
    }

    public void StopLogMode()
    {
        x = 0; z = 0; cog = 0; sr = 0;
        upDownSlider.value = 0;
        leftRightSlider.value = 0;
        logs.StopLogMode();
    }

    public void updateLogs()
    {
        logs.Update(x, z, cog, rot, sr, velocity);
    }

}
